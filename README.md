# Electron Updater Robby

Simple update manager for Electron applications. 

This package is part of a Robbyson Systems S/A applications and is distributed under MIT licence.

### Simple Implementation
```js
var Updater = require('electron-updater-robby');

Updater.create(
            {
                width: 460, // Update window width
                height: 230, //Update window height
                title: 'Update',//Update window title
                updateServerUrl: 'http://localhost:3000', //Update Server address
                checkInterval: 10000, //Update server check interval 
                parentWindow: mainWindow, //Electron main application window
                showDevTools: false, //Use to debug the update page on DevTools
                downloadMode: 'electron', // "request": has a progress bar support and don't support proxy. "electron": Has proxy support and but not the progress bar 
                updateWindow: {
                    applicationName: 'Your Application name' //Application name to display at update window
                }
            }
        );

Updater.watch();// Start update check

```

### Update Server structure
```
/version.json
/update-file.zip
```

### Version.json structure

```json
{
    "version": "1.0.0",//Version Number
    "file": "update-file.zip",//update zip file
    "cache": "f8a8c52b6dfc82a05b2b523bd7347a0cdb9b2e97", //cache Id use to force a AppData clean
    "restart": false //Restart application after update
}
```