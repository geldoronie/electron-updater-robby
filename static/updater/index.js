const ipcRenderer = require('electron').ipcRenderer;
var updater = {
    updating: false
};

$(function() {
    $("#progressbar").progressbar({
        value: 0
    });
    const updateButton = document.getElementById('update-button');
    const closeUpdateButton = document.getElementById('close-button');
    updateButton.addEventListener('click', (event) => {
        updater.start();
    });

    closeUpdateButton.addEventListener('click', (event) => {
        updater.close();
    });

    ipcRenderer.on('download-update-total', (event, value) => {
        console.log(value);
        $("#progressbar").progressbar("option", "max", value);
    });

    ipcRenderer.on('download-update-progress', (event, value) => {
        console.log(value);
        $("#progressbar").progressbar("option", "value", value);
    });

    ipcRenderer.on('download-update-error', (event, value) => {
        console.log(value);
        $("#progressbar-container").hide();
        $("#update-error-container").show();
    });
});

updater.start = function() {
    ipcRenderer.send('download-update');
    $("#progressbar-container").show();
    $("#update-button-container").hide();
};

updater.close = function() {
    ipcRenderer.send('close-updater');    
};