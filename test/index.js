
const express = require('express');
var Updater = require('../updater.js');
if (require.main !== module) {
    require('update-electron-app')({
        logger: require('electron-log')
    })
}
const fs = require('fs');
const path = require('path');
const { ipc, app, BrowserWindow } = require('electron');
let defaultWindowOpts = require('electron-browser-window-options');

const debug = /--debug/.test(process.argv[2])

if (process.mas) app.setName('Electron Updater ')

let mainWindow = null

var webserver = express();
webserver.use(express.static('./update'));

function initialize() {    

    function createWindow() {
        var windowOptions = Object.assign({}, defaultWindowOpts, {
            width: 1000,
            minWidth: 680,
            height: 680,
            title: app.getName()
        });
        
        mainWindow = new BrowserWindow(windowOptions)          
        Updater.create(
            {
                width: 460,
                height: 230,
                title: 'Update',
                updateServerUrl: 'http://localhost:3000',
                checkInterval: 10000,
                customUpdatePageUrl: app.getAppPath() + '/../',
                parentWindow: mainWindow,
                updateWindow: {
                    applicationName: 'Test Application'
                }
            }
        );
        Updater.watch();
        
        mainWindow.webContents.on('did-finish-load', () => {            
            
        });

        mainWindow.webContents.once('dom-ready', () => {

        });

        mainWindow.webContents.on('did-navigate', () => {
            mainWindow.webContents.on('did-get-response-details', function() {

            });
        });
        
        mainWindow.loadURL(path.join('http://', 'localhost:3000','index.html'));

        mainWindow.maximize()
        if (debug) {
            mainWindow.webContents.openDevTools()            
            require('devtron').install()
        }

        mainWindow.on('closed', () => {
            mainWindow = null
        })
    }

    app.on('ready', () => {
        createWindow()
    })

    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit()
        }
    })

    app.on('activate', () => {
        if (mainWindow === null) {
            createWindow()
        }
    })
}

initialize();
webserver.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});