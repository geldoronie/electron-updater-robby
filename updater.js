const fs = require('fs');
const path = require('path');
const request = require('request');
const progress = require('request-progress');
const unzip = require('unzip2');
const { ipcRenderer, ipcMain, BrowserWindow, app } = require('electron');
const { download } = require('electron-dl');
const glob = require("glob");
let defaultWindowOpts = require('electron-browser-window-options');

let applicationRootPath = '';
let windowUpdaterOptions = {
    applicationName: 'Application Name',
    errorMessage: 'The update was failed.',
    newVersionMessage: 'A new version is avaliable.',
    updateButtonLabel: 'Update',
    closeButtonLabel: 'Close'
};

let updaterOptions = {
    width: 460,
    height: 230,
    title: '',
    updateServerUrl: '',
    customUpdatePageUrl: './node_modules/electron-updater-robby/',
    checkInterval: 10000,
    parentWindow: null,
    updateWindow: {}
};

let updater = {
    title: '',
    window: null,
    parent: null,
    current: null,
    new: null,
    watcher: null,
    modalPromiseResolve: () => {},
    modalPromiseReject: () => {}
};

updater.create = (options) => {
    updaterOptions = Object.assign(updaterOptions, options);
    updaterOptions.updateWindow = Object.assign(windowUpdaterOptions, options.updateWindow);
    applicationRootPath = app.getAppPath();
    updater.parent = options.parentWindow;
    updater.title = options.title;

    var windowOptions = Object.assign({}, defaultWindowOpts, {
        width: options.width, //460,
        height: options.height, //230,
        title: updater.title,
        resizable: false,
        movable: false,
        minimizable: false,
        maximizable: false,
        modal: true,
        center: true,
        titleBarStyle: 'hidden',
        backgroundColor: '#fff',
        parent: updater.parent,
        downloadMode: 'request', //request,electron
        showWebTools: false,
        //frame: false,
        webPreferences: {
            webSecurity: false
        }
    });

    updater.window = new BrowserWindow(windowOptions);
    updater.window.hide();
    if (updaterOptions.showWebTools)
        updater.window.webContents.openDevTools()
    updater.window.setMenuBarVisibility(false);

    updater.window.webContents.on('did-finish-load', () => {
        updater.injections();
    });

    updater.window.webContents.once('dom-ready', () => {

    });

    updater.window.webContents.on('did-navigate', () => {
        updater.window.webContents.on('did-get-response-details', function() {

        });
    });

    updater.window.on('closed', () => {
        updater.parent.close();
    });
};

updater.watch = () => {
    updater.watcher = setInterval(() => {
        let getUpdateFunc = null;

        if (updaterOptions.downloadMode === 'request')
            getUpdateFunc = updater.getUpdate;
        else if (updaterOptions.downloadMode === 'electron')
            getUpdateFunc = updater.getUpdateOverElectron;
        else
            throw 'Invalid updater download mode';
        getUpdateFunc().then(() => {
            if (updater.hasUpdate()) {
                updater.open();
            }
            if (updater.hasNewCache()) {
                updater.saveVersionFile().then(updater.clearAppData).catch(ex => {
                    throw ex;
                });
            }
        }).catch(ex => {
            throw 'Failed to get the update info version.json.';
        });
    }, updaterOptions.checkInterval);
};

updater.stopWatch = () => {
    clearInterval(updater.watcher);
};

updater.getUpdateOverElectron = () => {
    return new Promise((resolve, reject) => {

        download(updater.window, updaterOptions.updateServerUrl + '/' + 'version.json', { directory: app.getAppPath(), filename: 'new_version.json' }).then(() => {
            if (fs.existsSync(path.join(app.getAppPath(), 'new_version.json'))) {
                fs.readFile(path.join(app.getAppPath(), 'new_version.json'), function(err, data) {
                    updater.new = JSON.parse(data);
                    if (fs.existsSync(path.join(app.getAppPath(), 'version.json'))) {
                        fs.readFile(path.join(app.getAppPath(), 'version.json'), function(err, data) {
                            if (err) {
                                console.log('Failed to get the local version');
                                resolve();
                                return;
                            }
                            console.log('A new version avaliable!');
                            updater.current = JSON.parse(data.toString());
                            resolve();
                            return;
                        });
                    } else {
                        console.log('A new version avaliable!');
                        resolve();
                        return;
                    }
                });
            }
        }).catch(() => {
            reject();
        });

    });
};

updater.getUpdate = () => {
    return new Promise((resolve, reject) => {
        request(updaterOptions.updateServerUrl + '/' + 'version.json', (error, res, body) => {
            if (error || !res || res.statusCode != 200) {
                reject();
                return;
            }
            updater.new = JSON.parse(body);
            if (fs.existsSync(path.join(app.getAppPath(), 'version.json'))) {
                fs.readFile(path.join(app.getAppPath(), 'version.json'), function(err, data) {
                    if (err) {
                        console.log('Failed to get the local version');
                        resolve();
                        return;
                    }
                    console.log('A new version avaliable!');
                    updater.current = JSON.parse(data.toString());
                    resolve();
                    return;
                });
            } else {
                console.log('A new version avaliable!');
                resolve();
                return;
            }
        });
    });
};

updater.hasUpdate = () => {
    if (!updater.current)
        return true;
    let currentVersion = parseInt(updater.current.version.replace(/\./g, ''));
    let newVersion = parseInt(updater.new.version.replace(/\./g, ''));
    if (currentVersion < newVersion)
        return true;
    else
        return false;
};

updater.hasNewCache = () => {

    if (updater.current && updater.current.cache != updater.new.cache) {
        return true;
    } else
        return false;
};

updater.open = () => {

    updater.stopWatch();
    updater.modalPromiseResolve = () => {};
    updater.modalPromiseReject = () => {};
    if (updater.parent)
        updater.parent.hide();
    updater.window.show();
    return new Promise((resolve, reject) => {
        updater.modalPromiseResolve = resolve;
        updater.modalPromiseReject = reject;
        if (updaterOptions.customUpdatePageUrl == './node_modules/electron-updater-robby/')
            updater.window.loadFile(path.join(updaterOptions.customUpdatePageUrl, 'static/updater/index.html'));
        else
            updater.window.loadFile(path.join(updaterOptions.customUpdatePageUrl));
    });
};


ipcMain.on('download-update', () => {
    if (updaterOptions.downloadMode === 'request')
        updater.downloadUpdate();
    else if (updaterOptions.downloadMode === 'electron')
        updater.downloadUpdateOverElectron();
    else
        throw 'Invalid updater download mode';
});

updater.downloadUpdate = () => {
    console.log('download-update');
    progress(request(updaterOptions.updateServerUrl + '/' + updater.new.file))
        .on('progress', state => {
            updater.window.webContents.send('download-update-progress', state.percent * 100);
        })
        .on('error', err => {
            updater.window.webContents.send('download-update-error', err);
        })
        .on('end', () => {
            updater.window.webContents.executeJavaScript('console.log("' + app.getAppPath() + '/' + updater.new.file + '");');
            if (fs.existsSync(app.getAppPath() + '/' + updater.new.file)) {
                updater.window.webContents.send('download-update-progress', 100);
                updater.extractPackage(app.getAppPath() + '/' + updater.new.file, app.getAppPath() + "/").then(() => {
                    let saveVersionFileFunc = null;
                    if (updaterOptions.downloadMode === 'request')
                        saveVersionFileFunc = updater.saveVersionFile;
                    else if (updaterOptions.downloadMode === 'electron')
                        saveVersionFileFunc = updater.saveVersionFileOverElectron;
                    else
                        throw 'Invalid updater download mode';

                    saveVersionFileFunc().then(() => {
                        console.log("Version sucessful updated");
                        updater.modalPromiseResolve();
                        updater.window.hide();
                        updater.clearAppData();
                        if (updater.new.restart)
                            updater.relaunchApp();
                        if (updater.parent)
                            updater.parent.show();
                        updater.watch();
                    });
                });
            } else {
                updater.window.webContents.send('download-update-error', "Download failed");
            }
        })
        .pipe(fs.createWriteStream(app.getAppPath() + "/" + updater.new.file));
};

updater.downloadUpdateOverElectron = () => {
    console.log('download-update');
    let files = glob.sync("*.zip");
    files.forEach((element) => {
        fs.unlinkSync(element);
    });
    _downloadProgress = (percent) => {
        if (percent != Infinity)
            updater.window.webContents.send('download-update-progress', percent.toFixed(1) * 100);
        else
            updater.window.webContents.send('download-update-progress', "Infinity");
    };

    download(updater.window, updaterOptions.updateServerUrl + '/' + updater.new.file, { directory: app.getAppPath(), filename: updater.new.file, onProgress: _downloadProgress }).then(() => {
        updater.window.webContents.executeJavaScript('console.log("' + app.getAppPath() + '/' + updater.new.file + '");');
        if (fs.existsSync(app.getAppPath() + '/' + updater.new.file)) {
            updater.window.webContents.send('download-update-progress', 100);
            updater.extractPackage(app.getAppPath() + '/' + updater.new.file, app.getAppPath() + "/").then(() => {
                updater.saveVersionFileOverElectron().then(() => {
                    console.log("Version sucessful updated");
                    updater.modalPromiseResolve();
                    updater.window.hide();
                    updater.clearAppData();
                    if (updater.new.restart)
                        updater.relaunchApp();
                    if (updater.parent)
                        updater.parent.show();
                    updater.watch();
                });
            });
        } else {
            updater.window.webContents.send('download-update-error', "Download failed");
        }
    }).catch(() => {
        updater.window.webContents.send('download-update-error', err);
    });
};

updater.extractPackage = (inputFile, outputFile) => {
    updater.window.webContents.executeJavaScript('console.log("' + inputFile + '");');
    updater.window.webContents.executeJavaScript('console.log("' + outputFile + '");');
    return new Promise((resolve, reject) => {
        fs.createReadStream(inputFile).on('end', () => {
            resolve();
        }).pipe(unzip.Extract({ path: outputFile }));
    });
};

updater.relaunchApp = () => {
    app.relaunch({ args: process.argv.slice(1).concat(['--relaunch']) });
    app.exit(0)
};


updater.saveVersionFile = () => {
    return new Promise((resolve, reject) => {
        request(updaterOptions.updateServerUrl + '/' + 'version.json', (error, res, body) => {

            if (error || !res || res.statusCode != 200) {
                reject();
                return;
            }

            fs.writeFile(path.join(app.getAppPath(), 'version.json'), body, function(err) {
                if (err) {
                    reject();
                    return console.log(err);
                }
                console.log("Version file sucessful updated");
                resolve();
            });
        });
    });
};

updater.saveVersionFileOverElectron = () => {
    return download(updater.window, updaterOptions.updateServerUrl + '/' + 'version.json', { directory: app.getAppPath(), filename: 'version.json' });
};

updater.clearAppData = () => {
    const { session } = updater.parent.webContents;
    session.clearStorageData(() => {
        session.clearCache(() => {
            updater.parent.reload();
        });
    });
};

updater.injections = () => {
    updater.window.webContents.executeJavaScript('$("#application-name").text("' + updaterOptions.updateWindow.applicationName + '")');
    updater.window.webContents.executeJavaScript('$("#update-error-label").text("' + updaterOptions.updateWindow.errorMessage + '")');
    updater.window.webContents.executeJavaScript('$("#update-avaliable-label").text("' + updaterOptions.updateWindow.newVersionMessage + '")');
    updater.window.webContents.executeJavaScript('$("#update-button").text("' + updaterOptions.updateWindow.updateButtonLabel + '")');
    updater.window.webContents.executeJavaScript('$("#close-button").text("' + updaterOptions.updateWindow.closeButtonLabel + '")');
};

module.exports = updater;